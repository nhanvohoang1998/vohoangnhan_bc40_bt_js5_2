// Bài 1: Tính thuế thu nhập cá nhân 
/**
 * input: Nhập vào các thông tin họ tên, số tiền thu nhập, số người phụ thuộc
 * handle: Do đề bài không ghi rõ yêu cầu tính thuế nên cũng có thể tính theo cách chia ra từng đoạn thuế Vd: 110tr => 60tr *0.05 + (110tr-60tr) * 0.1
 * output: Xuất ra số tiền thuế thu nhập cá nhân
 */
function den60(tongThuNhapNam) {
    return (tongThuNhapNam * 0.05);
};
function tren60Den120(tongThuNhapNam) {
    return (60000000 * 0.05 + (tongThuNhapNam - 60000000) * 0.1);
};
function tren120Den210(tongThuNhapNam) {
    return (60000000 * 0.05 + 60000000 * 0.1 + (tongThuNhapNam - 120000000) * 0.15);
};
function tren210Den384(tongThuNhapNam) {
    return (60000000 * 0.05 + 60000000 * 0.1 + 90000000 * 0.15 + (tongThuNhapNam - 210000000) * 0.2);
};
function tren384Den624(tongThuNhapNam) {
    return (60000000 * 0.05 + 60000000 * 0.1 + 90000000 * 0.15 + 174000000 * 0.2 + (tongThuNhapNam - 384000000) * 0.25);
};
function tren624Den960(tongThuNhapNam) {
    return (60000000 * 0.05 + 60000000 * 0.1 + 90000000 * 0.15 + 174000000 * 0.2 + 240000000 * 0.25 + (tongThuNhapNam - 624000000) * 0.3);
};
function tren960(tongThuNhapNam) {
    return (60000000 * 0.05 + 60000000 * 0.1 + 90000000 * 0.15 + 174000000 * 0.2 + 240000000 * 0.25 + 336000000 * 0.3 + (tongThuNhapNam - 960000000) * 0.35);
};

function nguoiPhuThuoc(soNguoiPhuThuoc) {
    return (soNguoiPhuThuoc * 1600000)
}

document.getElementById("tinhTienThue").onclick = function () {
    const nhapHoTen = document.getElementById('nhapHoTen').value;
    const tongThuNhapNam = document.getElementById('tongThuNhapNam').value * 1;
    const soNguoiPhuThuoc = document.getElementById('soNguoiPhuThuoc').value * 1;
    const ketQua1 = document.getElementById('ketQua1');

    var thuNhapChiuThue = tongThuNhapNam - 4000000 - soNguoiPhuThuoc * 1600000;
    var tinhThuNhapChiuThue = 0;


    if (tongThuNhapNam > 0 && tongThuNhapNam <= 60000000) {
        tinhThuNhapChiuThue = den60(thuNhapChiuThue);
    } else if (tongThuNhapNam <= 120000000) {
        tinhThuNhapChiuThue = tren60Den120(thuNhapChiuThue);
    } else if (tongThuNhapNam <= 210000000) {
        tinhThuNhapChiuThue = tren120Den210(thuNhapChiuThue);
    }
    else if (tongThuNhapNam <= 384000000) {
        tinhThuNhapChiuThue = tren210Den384(thuNhapChiuThue);
    } else if (tongThuNhapNam <= 624000000) {
        tinhThuNhapChiuThue = tren384Den624(thuNhapChiuThue);
    } else if (tongThuNhapNam <= 960000000) {
        tinhThuNhapChiuThue = tren624Den960(thuNhapChiuThue);
    } else if (tongThuNhapNam > 960000000) {
        tinhThuNhapChiuThue = tren960(thuNhapChiuThue);
    }


    ketQua1.innerHTML = ` Họ tên: ${nhapHoTen}; Tiền thuế thu nhập cá nhân: ${new Intl.NumberFormat().format(tinhThuNhapChiuThue)}`
};

// Bài 2: Tính tiền cáp 
/**
 * input: nhập vào chọn loại khách hàng, mã khách hàng, số kênh cao cấp
 * handle:
 * output: Xuất ra số tiền cáp phải trả
 */
function tinhCap(chonLoaiKH, soKenhCaoCap) {
    if (chonLoaiKH == 1) {
        return (4.5 + 20.5 + soKenhCaoCap * 7.5);
    } else if (chonLoaiKH == 2) {
        const soKetNoi = document.getElementById('soKetNoi').value * 1;
        if (soKetNoi <= 10 && soKetNoi >= 0) {
            return (15 + 75 + 50 * soKenhCaoCap);
        } else if (soKetNoi > 10) {
            return (15 + 75 + (soKetNoi - 10) * 5 + 50 * soKenhCaoCap);
        };
    }
}

var chonLoaiKH = document.getElementById("chonLoaiKH");
chonLoaiKH.onchange = function () {
    if (chonLoaiKH.value == 2) {
        document.getElementById("divSoKetNoi").innerHTML = `<input type="number" class="form-control" id="soKetNoi" placeholder="Số kết nối">`
    } else {
        document.getElementById("divSoKetNoi").innerHTML = ``
    };
};

document.getElementById("tinhTienCap").onclick = function () {
    const nhapMaKH = document.getElementById('nhapMaKH').value;
    const soKenhCaoCap = document.getElementById('soKenhCaoCap').value * 1;
    var recallChonLoaiKH = chonLoaiKH.value;
    const ketQua2 = document.getElementById('ketQua2');
    var tinhTien = 0
    if (Number(recallChonLoaiKH) == 0) {
        alert("Hãy chọn loại khách hàng")
    } else {
        tinhTien = tinhCap(recallChonLoaiKH, soKenhCaoCap) * 1;
    }
    ketQua2.innerHTML = `Mã khách hàng: ${nhapMaKH}; Tiền cáp: $${new Intl.NumberFormat().format(tinhTien)}`
};